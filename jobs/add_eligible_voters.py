#!/usr/bin/env python3

import sys
import argparse
from typing import List, Optional

from overrides import overrides

from jobs import Job
from membership.database.base import Session
from membership.database.models import Election, EligibleVoter, Meeting
from membership.repos import MeetingRepo
from membership.services import AttendeeService, MemberService
from membership.services.eligibility import SanFranciscoEligibilityService


class AddEligibleVoters(Job):
    @overrides
    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        parser.add_argument('-e', type=int)
        parser.add_argument('-o', '--output', type=argparse.FileType('w'), default=sys.stdout)

    @overrides
    def run(self, config: dict) -> None:
        election_id: Optional[int] = config.get('e')
        out: IO[str] = config.get('output', sys.stdout)

        session = Session()
        election = None if election_id is None else session.query(Election).get(election_id)

        eligible_member_ids = self.eligible_member_ids(session, election)

        out.write('%d members found: ' % len(eligible_member_ids))
        out.write(', '.join(map(str, eligible_member_ids)) + "\n")
        if election is None:
            return
        resp = input('Add to %s? (Y/n) ' % election.name)
        if not resp or resp.lower() != 'y':
            return

        self.add_voters(election_id, eligible_member_ids, session)

    def eligible_member_ids(self, session: Session, election: Optional[Election]) -> List[int]:
        meeting_repository = MeetingRepo(Meeting)
        attendee_service = AttendeeService()
        eligibility_service = SanFranciscoEligibilityService(
            meetings=meeting_repository,
            attendee_service=attendee_service
        )
        member_service = MemberService(eligibility_service)

        members_result = member_service.all(session)
        eligible_members = eligibility_service.members_as_eligible_to_vote(
            session,
            members_result.members
        )

        if election is None:
            existing_member_ids = {}
        else:
            existing_eligible_voters = session \
                .query(EligibleVoter).filter(EligibleVoter.election == election).all()
            existing_member_ids = {voter.member_id for voter in existing_eligible_voters}

        return [
            member.id
            for member in eligible_members
            if member.is_eligible and member.id not in existing_member_ids
        ]

    def add_voters(self, election_id: int, member_ids: List[int], session: Session) -> None:
        for member_id in member_ids:
            eligible_voter = EligibleVoter(
                member_id=member_id,
                election_id=election_id,
                voted=False
            )
            session.add(eligible_voter)
        session.commit()
