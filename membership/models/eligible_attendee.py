from .eligible_member import MemberAsEligibleToVote


class AttendeeAsEligibleToVote(MemberAsEligibleToVote):

    def __init__(self, attendee, message):
        super().__init__(
            attendee.member,
            is_eligible=bool(attendee.eligible_to_vote),
            message=message
        )
        self.meeting = attendee.meeting
