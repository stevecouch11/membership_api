import json
import logging
from typing import Any, Dict, List

import pkg_resources
import requests

import membership
from config import EMAIL_API_KEY, EMAIL_DOMAIN, MAILGUN_URL, USE_EMAIL, PORTAL_URL, \
    SUPPORT_EMAIL_ADDRESS
from membership.database.models import Email, Member, Meeting, ProxyTokenState, ProxyToken


def send_member_emails(
        sender: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]],
        tag: str = '') -> None:
    """
    Create an email campaign from the sender using the given MailGun email template formatted string
    and substitute the recipient variables for every email key in the dict.

    NOTE: If `recipient_variables` is empty, then the email is sent to nobody, otherwise the email
    is sent to every key of the dictionary given.

    :param sender: the from field of the email to be received
    :param subject: the subject line of the email
    :param email_template: the full template with all variables / recipient variables declared
    :param recipient_variables: all the recipients of this email by email address with
                                all variables associated with this address in the values
    """
    filtered_recipient_variables = {
        member.email_address: obj for member, obj in recipient_variables.items()
        if not member.do_not_email
    }
    email_template = f'{email_template}\n\n\n to unsubscribe click here: %tag_unsubscribe_url%'
    send_emails(
        sender,
        subject,
        email_template,
        filtered_recipient_variables,
        list(filtered_recipient_variables.keys())
    )


def send_welcome_email(member: Member, verify_url: str) -> None:
    # send a welcome email with the email verification / password reset link from Auth0
    sender = f"DSA SF Tech Committee <{SUPPORT_EMAIL_ADDRESS}>"
    if verify_url is None:
        filename = 'templates/welcome_email.html'
    else:
        filename = 'templates/welcome_email_verification.html'
    template = pkg_resources.resource_string(membership.__name__, filename) \
        .decode("utf-8") \
        .format(PORTAL_URL=PORTAL_URL)

    recipient_variables = {member: {'name': member.first_name, 'link': verify_url}}
    send_member_emails(
        sender,
        'Welcome to the DSA SF Membership Portal, %recipient.name%!',
        template,
        recipient_variables
    )


def send_emails(
    sender: str,
    subject: str,
    email_template: str,
    recipient_variables: Dict[str, Dict[Any, str]],
    to_emails: List[str],
    tag: str = ''
):
    url = f'https://api.mailgun.net/v3/{EMAIL_DOMAIN}/messages'
    payload = [
        ('from', sender),
        ('recipient-variables', json.dumps(recipient_variables)),
        ('subject', subject),
        ('html', email_template),
        ('o:tag', tag)
    ]
    payload.extend([('to', email) for email in to_emails])
    if USE_EMAIL:
        r = requests.post(url, data=payload, auth=('api', EMAIL_API_KEY))
        if r.status_code > 299:
            logging.error(r.text)


def send_committee_request_email(name: str, member_email: str, email: str):
    sender = f"DSA SF Tech Committee <{SUPPORT_EMAIL_ADDRESS}>"
    # Something weird is happening where the field is
    # not being populated via the recipient variables on prod only.
    # To deal with this the template is inlined here.
    template = f'''
    <html>
    <body>
    <p>Dear co-chairs,</p>
    <p>A member, {name}, is requesting to be added to your list of active members.</p>
    <p>If this member is active in your committee, please go to your committee's
    page in the <a href="{PORTAL_URL}">DSA SF Membership Portal</a> and mark them as active.</p>
    <p>If this is an incorrect request, you can ignore this or
    contact the member at {member_email}.</p>
    <p>If you have any questions, please email
    <a href="mailto:tech@dsasf.org">tech@dsasf.org</a></p>
    <p>Solidarity,</p>
    <p>DSA SF Tech</p>
    </body>
    </html>'''
    recipient_variables = {email: {'name': name}}
    send_emails(
        sender,
        'A member is requesting membership in your committee',
        template,
        recipient_variables,
        [email]
    )


def send_proxy_nomination_action(
    meeting: Meeting,
    nominating_member: Member,
    receiving_member: Member,
    proxy_token: ProxyToken,
) -> None:
    sender = f"DSA SF Tech Committee <{SUPPORT_EMAIL_ADDRESS}>"
    verb = proxy_token.state
    if verb == ProxyTokenState.ACCEPTED:
        filename = 'templates/proxy_nomination_accepted.html'
    else:
        filename = 'templates/proxy_nomination_rejected.html'
    template = pkg_resources.resource_string(membership.__name__, filename) \
        .decode("utf-8") \
        .format(PORTAL_URL=PORTAL_URL)

    recipient_variables = {
        nominating_member.email_address: {
            'name': nominating_member.first_name,
            'actor_name': receiving_member.first_name,
            'meeting_name': meeting.name,
            'verb': verb.friendly_name,
            'token_link': proxy_token.url,
        },
    }
    subject = (
        '%recipient.actor_name% has %recipient.verb%'
        ' your proxy nomination for %recipient.meeting_name%'
    )
    send_emails(
        sender,
        subject,
        template,
        recipient_variables,
        [nominating_member.email_address],
    )


def update_email(email: Email) -> str:
    """
    Create or update routes in Mailgun
    :param email: the incoming email address (should have a least one forwarding address)
    :return: the external_id from mailgun
    """
    payload = [
        ('priority', 0),
        ('description', 'Forwarding rule for {address}'.format(address=email.email_address)),
        ('expression', 'match_recipient("{address}")'.format(address=email.email_address)),
    ]
    payload.extend([('action', 'forward("{address}")'.format(address=forward.forward_to))
                    for forward in email.forwarding_addresses])
    if email.external_id:
        r = requests.put(MAILGUN_URL + '/' + email.external_id,
                         data=payload,
                         auth=('api', EMAIL_API_KEY))
        if r.status_code > 299:
            raise Exception('Mailgun api failed to update email.')
        return email.external_id
    else:
        r = requests.post(MAILGUN_URL, data=payload, auth=('api', EMAIL_API_KEY))
        if r.status_code > 299:
            raise Exception('Mailgun api failed to create email.')
        return r.json().get('route').get('id')


def remove_email(external_id: str) -> None:
    r = requests.delete(MAILGUN_URL + '/' + external_id, auth=('api', EMAIL_API_KEY))
    if r.status_code > 299:
        raise Exception('Mailgun api failed to delete email.')
