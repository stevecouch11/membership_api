import logging
import random
import dateutil.parser
from typing import Optional, Union

from membership.database.base import Session
from membership.database.models import Meeting
from membership.services.errors import ValidationError

logger = logging.getLogger(__name__)


class MeetingService:

    class ErrorCodes:
        NAME_TAKEN = "NAME_TAKEN"
        INVALID_CODE = "INVALID_CODE"

    def add_meeting(self, name: str, committee_id: Optional[int], session: Session) -> Meeting:
        if session.query(Meeting).filter_by(name=name).count() > 0:
            raise ValidationError(
                MeetingService.ErrorCodes.NAME_TAKEN,
                'A meeting with this name already exists.'
            )

        meeting = Meeting(name=name, committee_id=committee_id)
        session.add(meeting)
        return meeting

    def find_meeting_by_id(self, meeting_id: str, session: Session) -> Optional[Meeting]:
        return session.query(Meeting).get(meeting_id)

    def set_meeting_fields(self, meeting: Meeting, json: dict, session: Session) -> Meeting:
        meeting.committee_id = json.get('committee_id')
        meeting.landing_url = json.get('landing_url')

        if 'code' in json:
            meeting = self.set_meeting_code(meeting, json['code'], session)

        if 'start_time' in json:
            if json['start_time'] is None:
                meeting.start_time = None
            else:
                meeting.start_time = dateutil.parser.parse(json['start_time'])
        if 'end_time' in json:
            if json['end_time'] is None:
                meeting.end_time = None
            else:
                meeting.end_time = dateutil.parser.parse(json['end_time'])

        session.add(meeting)
        session.commit()
        return meeting

    def set_meeting_code(self,
                         meeting: Meeting,
                         code: Optional[Union[int, str]],
                         session: Session) -> Meeting:
        if code is None:
            meeting.short_id = None
        elif code == 'autogenerate':
            meeting.short_id = self.generate_meeting_code(session)
        else:
            try:
                short_id = int(code)
            except (TypeError, ValueError):
                raise ValidationError(
                    MeetingService.ErrorCodes.INVALID_CODE,
                    f'Invalid meeting code: {repr(code)}.'
                    ' Expected 4 digit integer, null, or "autogenerate".'
                )
            if short_id < 1000 or short_id > 9999:
                raise ValidationError(
                    MeetingService.ErrorCodes.INVALID_CODE,
                    f'Invalid meeting code: {short_id}. Must be 4 digits long.'
                )
            meeting.short_id = short_id
        return meeting

    def generate_meeting_code(self, session: Session) -> int:
        for _ in range(5):
            short_id = random.randint(1000, 9999)
            if session.query(Meeting).filter_by(short_id=short_id).count() == 0:
                return short_id

        raise Exception('Failed to find an unused meeting code after five tries.')
