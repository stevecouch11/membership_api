alembic==1.0.10
blinker==1.4
Flask==0.12.2
Flask-Cors==3.0.2
jsonschema==2.6.0
mysqlclient==1.4.2
overrides==1.9
phonenumbers==8.10.14
# psycopg2cffi==2.7.4  # Uncomment for postgresql support.
pycryptodomex==3.6.4
PyJWT==1.5.0
python-dateutil==2.6.1
pytz==2018.9
raven==6.1.0
requests==2.17.3
SQLAlchemy==1.3.4
transitions==0.6.1
