import os
from typing import Callable, Dict, Iterable, Optional, Union, Type, TypeVar

V = TypeVar('V')
"""A type variable to represent the type of the converted config value"""

E = TypeVar('E')
"""A type variable to represent the type of the default value when the config key cannot be found"""


class ConfigThrow:
    """A sentinel value to indicate that the function will throw a ConfigError"""

    def __str__(self):
        return '<throw>'


throw = ConfigThrow()

OrElse = Union[E, Exception, type(throw), Callable[[], E]]
"""
The type of values that can be passed to the `or_else` clause of a config.

- If the value is an instance of Exception, it will be raised.
- If the value is `throw`, then a relevant `ConfigError` will be raised.
- If the value is a Callable that returns the intended type, it will be run at init time.
- Otherwise, the value will be returned instead of the converted value.
"""

SentinelResolver = Union[Callable[[str], V], type(throw), V]
"""
The type of values that can be passed to handle a config value matching one or none of the given
sentinel values.
"""


def convert_with_sentinel(
        on_sentinel: SentinelResolver,
        sentinels: Iterable[str],
        or_else: SentinelResolver = throw) -> Callable[[str], V]:
    """
    Returns a convert function that either returns a normalized value, throws and exception, or
    executes a given function to return a final result.

    :param on_sentinel: either a value, the throw object to raise a ConfigException when the config
                        value matches one of the given sentinels, or a callable that received the
                        raw config value and returns a result
    :param sentinels: converted to a set of lowercase string values that will trigger the
                      on_sentinel behavior
    :param or_else: a value to return, a callable that will receive the raw config value and return
                    a result, or the throw object (default) to throw an exception when the config
                    value does not match any of the sentinels
    :return: the convert function
    """
    normalized_sentinels = {s.lower() for s in sentinels}

    def converter(value: str) -> V:
        if value.lower() in normalized_sentinels:
            if on_sentinel is throw:
                raise ConfigValueError(
                    f"value {repr(value)} must NOT match any of {normalized_sentinels}"
                    " (case ignored)"
                )
            elif callable(on_sentinel):
                return on_sentinel(value)
            else:
                return on_sentinel
        else:
            if or_else is throw:
                raise ConfigValueError(
                    f"value {repr(value)} must match one of {normalized_sentinels} (case ignored)"
                )
            elif callable(or_else):
                or_else(value)
            else:
                return or_else
    return converter


true_values = {'true', '1', 'yes', 'on'}
"""true sentinel values"""

false_values = {'false', '0', 'no', 'off'}
"""false sentinel values"""

convert_bool: Callable[[str], bool] = convert_with_sentinel(
    lambda v: v.lower() in true_values,
    true_values.union(false_values),
    or_else=throw
)
"""
Converts the given value to True or False if it matches one of the sentinel values,
otherwise it throws an exception if it matches neither True or False sentinel values.
"""

convert_bool_or_true: Callable[[str], bool] = convert_with_sentinel(
    False,
    false_values,
    or_else=True
)
"""
Converts the given value to False if it matches one of the false sentinel values,
otherwise always returns True.
"""

convert_bool_or_false: Callable[[str], bool] = convert_with_sentinel(
    True,
    true_values,
    or_else=False
)
"""
Converts the given value to True if it matches one of the true sentinel values,
otherwise always returns False.
"""


class Config:
    """
    A wrapper around a `dict` of `(str, str)` that parses strings into typed values or throws
    helpful exceptions.
    """

    def __init__(self, src: Dict[str, str]):
        self.src = src

    @staticmethod
    def validate(
            as_type: Type[V],
            key: str,
            value: Optional[str],
            or_else: OrElse = throw,
            convert: Optional[Callable[[str], V]] = None) -> Union[V, E]:
        if value is None:
            if or_else is throw:
                raise ConfigKeyError(f"Cannot find key '{key}' from config")
            elif isinstance(or_else, Exception):
                raise or_else
            else:
                return or_else
        else:
            try:
                return as_type(value) if convert is None else convert(value)
            except Exception as e:
                raise ConfigValueError(
                    f"Could not load config key '{key}' as {as_type.__name__}: {e}"
                ) from e

    def get_as(
            self,
            as_type: Type[V],
            key: str,
            or_else: OrElse = throw,
            convert: Optional[Callable[[str], V]] = None) -> Union[V, E]:
        """
        Try to extract the config value found at the given key; if present, call either the custom
        convert function or pass the value to the given type object.

        :param as_type: the type of value to return (used to convert if no convert function given)
        :param key: the key to look up in the config map
        :param or_else: the value to return, callable that received the raw config and produces a
                        result value, or the throw object (default) to indicate that this should
                        raise a `ConfigError`
        :param convert: a function used to convert the raw config value to the expected result type
        :return: the config value as the expected type `V` otherwise the or_else value or callable
                 applied with the raw config value, or an exception is raised (default)
        """
        result = self.src.get(key, None)
        return self.validate(as_type, key, result, or_else, convert)

    def get_str(
            self,
            key: str,
            or_else: OrElse = throw,
            convert: Callable[[str], E] = str) -> Union[str, E]:
        """
        Returns the config value as a string, either raw or as returned from a convert function,
        otherwise returns the given or_else value or raises a `ConfigError`.

        See get_as() for more details.
        """
        return self.get_as(str, key, or_else, convert)

    def get_int(
            self,
            key: str,
            or_else: OrElse = throw,
            convert: Callable[[str], E] = int) -> Union[int, E]:
        """
        Returns the config value as an integer, either raw or as returned from a convert function,
        otherwise returns the given or_else value or raises a `ConfigError`.

        See get_as() for more details.
        """
        return self.get_as(int, key, or_else, convert)

    def get_float(
            self,
            key: str,
            or_else: OrElse = throw,
            convert: Callable[[str], E] = float) -> Union[float, E]:
        """
        Returns the config value as an float, either raw or as returned from a convert function,
        otherwise returns the given or_else value or raises a `ConfigError`.

        See get_as() for more details.
        """
        return self.get_as(float, key, or_else, convert)

    def get_bool(
            self,
            key: str,
            or_else: OrElse = throw,
            convert: Callable[[str], E] = convert_bool) -> Union[bool, E]:
        """
        Returns the config value as a boolean, either raw or as returned from a convert function,
        otherwise returns the given or_else value or raises a `ConfigError`.

        See get_as() and convert_bool for more details.
        """
        return self.get_as(bool, key, or_else, convert)

    def __str__(self):
        keys = set(self.src.keys()[:20])
        return f'Config({keys})'

    def __repr__(self):
        return f'Config({repr(self.src)})'


from_env: Config = Config(os.environ)


class ConfigError(Exception):
    """
    Exception thrown when a config key is missing or the value cannot be converted to the
    correct type.
    """


class ConfigKeyError(ConfigError):
    """
    Exception thrown when a config key is missing.
    """


class ConfigValueError(ConfigError):
    """
    Exception thrown when a config value cannot be converted to the expected type with the
    given conversion method.
    """
