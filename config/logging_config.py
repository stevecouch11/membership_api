import logging
from typing import Callable

from config import from_env

validate_log_level: Callable[[str], int] = logging._checkLevel  # NOQA

LOGGING_ROOT_LEVEL: str = from_env.get_str(
    'LOGGING_ROOT_LEVEL',
    logging.DEBUG,
    convert=validate_log_level,
)
